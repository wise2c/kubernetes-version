gcr.io/google_containers/etcd-amd64:3.3.10
gcr.io/google_containers/pause-amd64:
k8s.gcr.io/kube-apiserver-amd64:v1.19.9
k8s.gcr.io/kube-controller-manager-amd64:v1.19.9
k8s.gcr.io/kube-scheduler:v1.19.9
k8s.gcr.io/kube-proxy-amd64:v1.19.9
k8s.gcr.io/k8s-dns-kube-dns:1.15.10
k8s.gcr.io/k8s-dns-dnsmasq-nanny:1.15.10
k8s.gcr.io/k8s-dns-sidecar:1.15.10
rel="dns-prefetch"href="https://user-images.githubusercontent.com/">
property="og:image"content="https://github.githubassets.com/images/modules/open_graph/github-logo.png">
property="og:image:type"content="image/png">
property="og:image:width"content="1200">
property="og:image:height"content="1200">
property="og:image"content="https://github.githubassets.com/images/modules/open_graph/github-mark.png">
property="og:image:type"content="image/png">
property="og:image:width"content="1200">
property="og:image:height"content="620">
property="og:image"content="https://github.githubassets.com/images/modules/open_graph/github-octocat.png">
property="og:image:type"content="image/png">
property="og:image:width"content="1200">
property="og:image:height"content="620">
property="twitter:card"content="summary_large_image">
property="twitter:image:src"content="https://github.githubassets.com/images/modules/open_graph/github-logo.png">
property="twitter:image:width"content="1200">
property="twitter:image:height"content="1200">
rel="alternateicon"
rel="icon"class="js-site-favicon"
src="https://github.githubassets.com/images/search-key-slash.svg"alt=""








src="https://github.githubassets.com/images/modules/site/icons/footer/github-logo.svg"width="84"
src="https://github.githubassets.com/images/modules/site/icons/footer/twitter.svg"height="18"
src="https://github.githubassets.com/images/modules/site/icons/footer/facebook.svg"height="18"
src="https://github.githubassets.com/images/modules/site/icons/footer/youtube.svg"height="16"
src="https://github.githubassets.com/images/modules/site/icons/footer/linkedin.svg"height="18"
src="https://github.githubassets.com/images/modules/site/icons/footer/github-mark.svg"height="20"
quay.io/coreos/flannel:-amd64