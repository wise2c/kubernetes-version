gcr.io/google_containers/etcd-amd64:
gcr.io/google_containers/pause-amd64:
k8s.gcr.io/kube-apiserver-amd64:v1.22.4
k8s.gcr.io/kube-controller-manager-amd64:v1.22.4
k8s.gcr.io/kube-scheduler:v1.22.4
k8s.gcr.io/kube-proxy-amd64:v1.22.4
quay.io/coreos/flannel:-amd64