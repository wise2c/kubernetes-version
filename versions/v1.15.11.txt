gcr.io/google_containers/etcd-amd64:
gcr.io/google_containers/pause-amd64:
k8s.gcr.io/kube-apiserver-amd64:v1.15.11
k8s.gcr.io/kube-controller-manager-amd64:v1.15.11
k8s.gcr.io/kube-scheduler:v1.15.11
k8s.gcr.io/kube-proxy-amd64:v1.15.11
quay.io/coreos/flannel:v0.12.0-amd64
k8s.gcr.io/coredns:1.3.1