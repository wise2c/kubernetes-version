gcr.io/google_containers/etcd-amd64:3.3.10
gcr.io/google_containers/pause-amd64:3.1
k8s.gcr.io/kube-apiserver-amd64:v1.13.7
k8s.gcr.io/kube-controller-manager-amd64:v1.13.7
k8s.gcr.io/kube-scheduler:v1.13.7
k8s.gcr.io/kube-proxy-amd64:v1.13.7
k8s.gcr.io/k8s-dns-kube-dns:1.14.13
k8s.gcr.io/k8s-dns-dnsmasq-nanny:1.14.13
k8s.gcr.io/k8s-dns-sidecar:1.14.13
k8s.gcr.io/kubernetes-dashboard-amd64:v1.10.1
quay.io/coreos/flannel:v0.11.0-amd64
k8s.gcr.io/coredns:1.2.6