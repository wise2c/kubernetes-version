gcr.io/google_containers/etcd-amd64:
gcr.io/google_containers/pause-amd64:
k8s.gcr.io/kube-apiserver-amd64:v1.18.12
k8s.gcr.io/kube-controller-manager-amd64:v1.18.12
k8s.gcr.io/kube-scheduler:v1.18.12
k8s.gcr.io/kube-proxy-amd64:v1.18.12
rel="dns-prefetch"href="https://user-images.githubusercontent.com/">
property="og:image"content="https://github.githubassets.com/images/modules/open_graph/github-logo.png">
property="og:image:type"content="image/png">
property="og:image:width"content="1200">
property="og:image:height"content="1200">
property="og:image"content="https://github.githubassets.com/images/modules/open_graph/github-mark.png">
property="og:image:type"content="image/png">
property="og:image:width"content="1200">
property="og:image:height"content="620">
property="og:image"content="https://github.githubassets.com/images/modules/open_graph/github-octocat.png">
property="og:image:type"content="image/png">
property="og:image:width"content="1200">
property="og:image:height"content="620">
property="twitter:card"content="summary_large_image">
property="twitter:image:src"content="https://github.githubassets.com/images/modules/open_graph/github-logo.png">
property="twitter:image:width"content="1200">
property="twitter:image:height"content="1200">
rel="alternateicon"
rel="icon"class="js-site-favicon"
src="https://github.githubassets.com/images/search-key-slash.svg"alt=""








quay.io/coreos/flannel:v0.13.0-amd64