gcr.io/google_containers/etcd-amd64:
gcr.io/google_containers/pause-amd64:
k8s.gcr.io/kube-apiserver-amd64:v1.25.0
k8s.gcr.io/kube-controller-manager-amd64:v1.25.0
k8s.gcr.io/kube-scheduler:v1.25.0
k8s.gcr.io/kube-proxy-amd64:v1.25.0
registry.k8s.io/dns/k8s-dns-kube-dns:1.22.8
registry.k8s.io/dns/k8s-dns-dnsmasq-nanny:1.22.8
registry.k8s.io/dns/k8s-dns-sidecar:1.22.8
lang="en"data-color-mode="auto"
rel="dns-prefetch"href="https://user-images.githubusercontent.com/">
property="og:image"content="https://github.githubassets.com/images/modules/open_graph/github-logo.png">
property="og:image:type"content="image/png">
property="og:image:width"content="1200">
property="og:image:height"content="1200">
property="og:image"content="https://github.githubassets.com/images/modules/open_graph/github-mark.png">
property="og:image:type"content="image/png">
property="og:image:width"content="1200">
property="og:image:height"content="620">
property="og:image"content="https://github.githubassets.com/images/modules/open_graph/github-octocat.png">
property="og:image:type"content="image/png">
property="og:image:width"content="1200">
property="og:image:height"content="620">
property="twitter:card"content="summary_large_image">
property="twitter:image:src"content="https://github.githubassets.com/images/modules/open_graph/github-logo.png">
property="twitter:image:width"content="1200">
property="twitter:image:height"content="1200">
rel="alternateicon"
rel="icon"class="js-site-favicon"








src="https://github.githubassets.com/images/modules/site/icons/footer/twitter.svg"height="18"
src="https://github.githubassets.com/images/modules/site/icons/footer/facebook.svg"width="18"
src="https://github.githubassets.com/images/modules/site/icons/footer/linkedin.svg"width="19"
src="https://github.githubassets.com/images/modules/site/icons/footer/youtube.svg"width="23"
src="https://github.githubassets.com/images/modules/site/icons/footer/twitch.svg"width="18"
src="https://github.githubassets.com/images/modules/site/icons/footer/tiktok.svg"width="18"
src="https://github.githubassets.com/images/modules/site/icons/footer/github-mark.svg"width="20"
quay.io/coreos/flannel:-amd64