k8s.gcr.io/kube-apiserver-amd64:v1.12.1
k8s.gcr.io/kube-controller-manager-amd64:v1.12.1
k8s.gcr.io/kube-scheduler-amd64:v1.12.1
k8s.gcr.io/kube-proxy-amd64:v1.12.1
k8s.gcr.io/pause:3.1
k8s.gcr.io/etcd-amd64:3.2.24
k8s.gcr.io/coredns:1.2.2
quay.io/coreos/flannel:v0.10.0-amd64